import '@/main.js'
import Vue from 'vue'
import App from './home.vue'

// 这里写不是全局的方法等
console.log('home.html')

new Vue({
  render: h => h(App)
}).$mount('#app')
